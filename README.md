# CarCar

Team:

* Katelyn Chang - Sales
* Christopher Bush - Service

## Design
CarCar is an application designed to help run a car dealership. It has microservices to run the dealership's service center and sales department and manage its inventory.

The application uses React to manage its front end, Django to manage the back end, and the PostgreSQL to manage the database.

![Img](/images/CarCarDiagram.png)

## How to Run this App

1. Grab the repository link to clone. You can find the Clone button on the right hand side of the Gitlab project page. If you are not set up with SSH copy the clone with HTTPS link.

    https://gitlab.com/Kat3lyn5/project-beta.git

2. Clone the repository using the line below in your terminal.

    git clone https://gitlab.com/Kat3lyn5/project-beta.git

3. Next we will create your database volume in docker. Enter the line below in your terminal.

    docker volume create beta-data

4. Then, we will build your containers and images.

    docker-compose build

5. Finally, we get Docker up and running.

    docker-compose up

6. Go into Docker, click containers and click the ">" next to project-beta. This will display all your containers so you can make sure their status is "Running".

7. Once everything is running, navigate to your browser and enter http://localhost:3000 to view the project.

## Service microservice

The Service microservice has 3 models, appointment, technician, and one for an automobile value object. The appointment model has a foreign key allowing it to access the technician model. The microservice has a poller that polls for data from the inventory API.

## Sales microservice

There are four models on the Sales microservice side:
AutomobileVO -vin, sold
Salesperson - first_name, last_name, employee_Id
Customer - first_name, last_name, phone_number, address
Sales - price, automobile, salesperson, customer

In order for a sale to be made AutomobileVO is used in the poller to check to see which vins are available by checking the inventory microservice. Once a vin is selected that has not yet been sold, a salesperson and customer is inputted then the sale can be created.

## API Documentation
Service Microservice:

Technician:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create a technician | POST | http://localhost:8080/api/technicians/
| List technicians | GET | http://localhost:8080/api/technicians/
| Show a specific technician | GET | http://localhost:8080/api/technicians/

To create a Technician (SEND THIS JSON BODY):
```
	{
			"first_name": "Marlo",
			"last_name": "Stanfield",
			"employee_id": "12",
	}
```

Appointment:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create an appointment | POST | http://localhost:8080/api/appointments/
| List all appointments | GET | http://localhost:8080/api/appointments/
| Show a specific appointment | GET | http://localhost:8080/api/appointments/

To create an appointment (SEND THIS JSON BODY):
```
	[INCOMPLETE]
```

Sales Microservice

Customer:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create a customer | POST | http://localhost:8090/api/customers/
| List customers | GET | http://localhost:8090/api/customers/
| Show a specific customer | GET | http://localhost:8090/api/customers/id/

To create a Customer (SEND THIS JSON BODY):
```
	{
			"first_name": "Selena",
			"last_name": "Gomez",
			"address": "1284 lemon lane",
			"phone_number": "123-456-9765"
	}
```
Return Value of Creating a Customer:
```
{
			"first_name": "Selena",
			"last_name": "Gomez",
			"address": "1284 lemon lane",
			"phone_number": "123-456-9765",
			"id": 2
	}
```

To list Customers:

```
Select No Body

```

Return value of Listing all Customers:
```
{
	"customers": [
		{
			"first_name": "Selena",
			"last_name": "Gomez",
			"address": "1284 lemon lane",
			"phone_number": "123-456-9765",
			"id": 2
		},
		{
			"first_name": "Trader",
			"last_name": "Joes",
			"address": "5674 fearless lane",
			"phone_number": "453-333-8523",
			"id": 4
		}
	]
}
```


To Show a specific Customer:

```
Select No Body and add Customer id to request link.
http://localhost:8090/api/customers/5

```

Return value of requesting a specific Customer:
```
{
	"customer": {
		"first_name": "Stephen ",
		"last_name": "Curry",
		"address": "3030 Curry lane.",
		"phone_number": "",
		"id": 5
	}
}
```

Delete a Customer:

To delete a customer (SEND THIS JSON BODY):
```
			{
			"first_name": "Michelle ",
			"last_name": "Obama",
			"address": "1000 Becoming Lane",
			"phone_number": "523-954-9632",
			"id": 7
		}
```
Return Value of delete a customer

```
{
	"deleted": true
}
```

### Salespeople:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Salesperson details | GET | http://localhost:8090/api/salesperson/id/
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/id/
​
To create a salesperson (SEND THIS JSON BODY):
```
{
	"first_name": "Adele",
	"last_name": "Sings",
	"employee_id": "EM-3"
}
```
Return Value of creating a salesperson:
```
{
	"first_name": "Adele",
	"last_name": "Sings",
	"employee_id": "EM-3",
	"id": 5
}
```
To Show a list of salespeople:

```
Select No Body

```

 Return Value of listing all salespeople:
```
{
	"salespeople": [
		{
			"first_name": "Brad",
			"last_name": "Pitt",
			"employee_id": "EM-2",
			"id": 2
		},
		{
			"first_name": "Adele",
			"last_name": "Sings",
			"employee_id": "EM-3",
			"id": 5
		},
	]
}
```
Salesperson details:
```
No JSON Body. Make sure to enter salesperson id in request link.

http://localhost:8090/api/salesperson/7/

```

Return Value of getting a specific salesperson details:
```
{
	"salesperson": {
		"first_name": "Klay",
		"last_name": "Thompson",
		"employee_id": "EM-11",
		"id": 7
	}
}
```

Delete a salesperson:

To delete a salesperson (SEND THIS JSON BODY):
```
		{
			"first_name": "Lebron",
			"last_name": "James",
			"employee_id": "EM-23",
			"id": 6
		}
```
Return Value of deleta salesperson

```
{
	"deleted": true
}
```

### Sales:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create a new sale | POST | http://localhost:8090/api/sales/
| List all sales | GET | http://localhost:8090/api/sales/
| Delete a sale | DELETE | http://localhost:8090/api/sales/:id

List all Salesrecords Return Value:

Create a New Sale (SEND THIS JSON BODY):
```
{
    "price": 60000,
    "customer": "2",
    "salesperson": "6",
    "automobile": "1LNHM87A31Y667552"

}
```

Return Value of Creating a New Sale:
```
{
	"sales": {
		"price": 60000,
		"automobile": {
			"vin": "1LNHM87A31Y667552",
			"sold": false
		},
		"salesperson": {
			"first_name": "Lebron",
			"last_name": "James",
			"employee_id": "EM-23",
			"id": 6
		},
		"customer": {
			"first_name": "Selena",
			"last_name": "Gomez",
			"address": "1284 lemon lane",
			"phone_number": "123-456-9765",
			"id": 2
		},
		"id": 7
	}
}
```

List all Sales

```
No JSON Body
```

Return Value of all Sales listings

```
{
	"sales": [
		{
			"price": 600000,
			"automobile": {
				"vin": "1LNHM87A31Y667552",
				"sold": false
			},
			"salesperson": {
				"first_name": "Brad",
				"last_name": "Pitt",
				"employee_id": "EM-2",
				"id": 2
			},
			"customer": {
				"first_name": "Trader",
				"last_name": "Joes",
				"address": "5674 fearless lane",
				"phone_number": "453-333-8523",
				"id": 4
			},
			"id": 4
		},
		{
			"price": 600000,
			"automobile": {
				"vin": "1LNHM87A31Y667552",
				"sold": false
			},
			"salesperson": {
				"first_name": "Adele",
				"last_name": "Sings",
				"employee_id": "EM-3",
				"id": 5
			},
			"customer": {
				"first_name": "Stephen ",
				"last_name": "Curry",
				"address": "3030 Curry lane.",
				"phone_number": "",
				"id": 5
			},
			"id": 5
		},
	]
}
```
Delete a Sale

To delete a sale (SEND THIS JSON BODY):
```
		{
			"price": 600000,
			"automobile": {
				"vin": "1LNHM87A31Y667552",
				"sold": false
			},
			"salesperson": {
				"first_name": "Brad",
				"last_name": "Pitt",
				"employee_id": "EM-2",
				"id": 2
			},
			"customer": {
				"first_name": "Trader",
				"last_name": "Joes",
				"address": "5674 fearless lane",
				"phone_number": "453-333-8523",
				"id": 4
			},
			"id": 4
		}
```
Return Value of delete a salesperson

```
{
	"deleted": true
}
```


### ValueObjects:

AutomobileVO - This was the value object for both the sales and services microserviece. As you can see in the diagram above AutomobileVO is present in both. On the sales side, it is used in the poller to poll data from inventory microservice to keep track of automobile sales with "vin" and "sold" statuses.
