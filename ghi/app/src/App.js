import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import React, {useState, useEffect} from 'react';
import SalespeopleForm from './SalespeopleForm';
import SalespeopleList from './SalespeopleList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SalesForm from './SalesForm';
import SalesList from './SalesList';
import SalespersonHistory from './SalespersonHistory';
import ManufacturerForm from './ManufacturerForm';
import ManufacturersList from './ManufacturerList';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import ModelForm from './ModelForm';



function App(props) {

const [customers, setCustomers] = useState([]);
const [salespeople, setSalespeople] = useState([]);
const [sales, setSales] = useState([]);
const [manufacturers, setManufacturers] = useState([]);
const [technicians, setTechnicians] = useState([]);
const [appointments, setAppointments] = useState([]);



async function LoadCustomers() {
  const response = await fetch('http://localhost:8090/api/customers/');
  if (response.ok) {
      const data = await response.json();
      console.log(data)
      setCustomers(data.customers);
  } else(
      console.error('An error occured fetching the data')
  )
}

async function LoadSalespeople() {
  const response = await fetch('http://localhost:8090/api/salespeople/');
  if (response.ok) {
    const data = await response.json();
    console.log(data)
    setSalespeople(data.salespeople);
} else(
    console.error('An error occured fetching the data')
)
}

async function LoadSales() {
  const response = await fetch('http://localhost:8090/api/sales/');
  if (response.ok) {
    const data = await response.json();
    console.log(data)
    setSales(data.sales);
} else(
    console.error('An error occured fetching the data')
)

}

async function LoadManufacturers() {
  const response = await fetch('http://localhost:8100/api/manufacturers/');
  if (response.ok) {
    const data = await response.json();
    console.log(data)
    setManufacturers(data.manufacturers);
} else(
    console.error('An error occured fetching the data')
)

}

async function LoadTechnicians() {
  const response = await fetch('http://localhost:8080/api/technicians/');
  if (response.ok) {
    const data = await response.json();
    console.log(data)
    setTechnicians(data.technicians);
} else(
  console.error('An error occured fetching the data')
)

}

async function LoadAppointments() {
  const response = await fetch('http://localhost:8080/api/appointments/');
  if (response.ok) {
    const data = await response.json();
    console.log(data)
    setAppointments(data.appointments);
} else(
  console.error('An error occured fetching the data')
)

}



useEffect(() => {
  LoadCustomers();
  LoadSalespeople();
  LoadSales();
  LoadManufacturers();
  LoadTechnicians();
  LoadAppointments();
}, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path = "salespeople">
            <Route index element = {<SalespeopleList/>}/>
            <Route path = "/salespeople/create" element = {<SalespeopleForm/>}/>
          </Route>
          <Route path = "customers">
            <Route index element = {<CustomerList/>}/>
            <Route path = "/customers/create" element = {<CustomerForm/>}/>
          </Route>
          <Route path = "sales">
            <Route index element = {<SalesList/>}/>
            <Route path = "/sales/create" element = {<SalesForm/>}/>
          </Route>
          <Route path = "salesperson/history">
            <Route index element = {<SalespersonHistory/>}/>
          </Route>
          <Route path = "technicians">
            <Route index element = {<TechnicianList/>}/>
            <Route path = "/technicians/create" element = {<TechnicianForm/>}/>
          </Route>
          <Route path = "appointments">
            <Route index element = {<AppointmentList/>}/>
            <Route path = "/appointments/create" element = {<AppointmentForm/>}/>
          </Route>
          <Route path = "manufacturers">
            <Route index element = {<ManufacturersList/>}/>
            <Route path = "/manufacturers/create" element = {<ManufacturerForm/>}/>
          </Route>
           <Route path = "/model/create" element = {<ModelForm/>}/>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App
