import React, {useState, useEffect} from 'react';

function AppointmentForm(props) {
    const[dateTime, setDateTime] = useState({ date: '', time: '' });
    const[vin, setVin] = useState('');
    const[reason, setReason] = useState('');
    const[technicianId, setTechnicianId] = useState('');
    const[technicians, setTechnicians] = useState([]);

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDateTime((prevDateTime) => ({
          ...prevDateTime,
          date: value,
        }));
    };

    const handleTimeChange = (event) => {
      const value = event.target.value;
      setDateTime((prevDateTime) => ({
        ...prevDateTime,
        time: value,
      }));
    };

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnicianId(value);
    }

    const getTechnicians = async() => {
      const response = await fetch('http://localhost:8080/api/technicians/')
      if (response.ok) {
        const data = await response.json();
        setTechnicians(data.technicians)
      }
    }
    useEffect(() => {
      getTechnicians();
    }, []);


    const handleSubmit = async(event) => {
        event.preventDefault();


        const formattedDate = dateTime.date;
        const formattedTime = dateTime.time;

        const formattedDateTime = `${formattedDate} ${formattedTime}`;

        const data = {};

        data.date_time = formattedDateTime;
        data.vin = vin;
        data.reason = reason;
        data.technician = technicianId;

    console.log(data)

    const appointmentUrl = 'http://localhost:8080/api/appointments/'
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
        const newAppointment = await response.json();
        // console.log(newAppointment);

        setDateTime('');
        setVin('');
        setReason('');
        setTechnicianId('');
    }
}

useEffect(() => {

  }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create Appointment</h1>
                <form onSubmit={handleSubmit} id="create-technician-form">
                  <div className="form-floating mb-3">
                    <input
                      onChange={handleDateChange}
                      value={dateTime.date}
                      placeholder="MM/DD/YYYY"
                      required
                      type="text"
                      name="Date"
                      id="Date"
                      className="form-control"/>
                    <label htmlFor="Date Time">Date</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                      onChange={handleTimeChange}
                      value={dateTime.time}
                      placeholder="HH:mm"
                      required
                      type="text"
                      name="Time"
                      id="Time"
                      className="form-control"/>
                    <label htmlFor="Time">Time (HH:mm)</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                    onChange = {handleVinChange}
                    value={vin}
                    placeholder="Vin"
                    required type="text"
                    name="Vin"
                    id="Vin"
                    className="form-control"/>
                    <label htmlFor="Vin">Automobile VIN</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                    onChange ={handleReasonChange}
                    value ={reason}
                    placeholder="Reason"
                    required type="text"
                    name="Reason"
                    id="Reason"
                    className="form-control"/>
                    <label htmlFor="Reason">Reason for Appointment</label>
                  </div>
                  <div className="form-floating mb-3">
                    <select
                      onChange={handleTechnicianChange}
                      value={technicianId}
                      required
                      placeholder="Technician"
                      name="Technician"
                      id="Technician"
                      className="form-select">
                      <option value="">Select a Technician</option>
                      {console.log(technicians)}
                      {technicians.length > 0 &&
                        technicians.map(technician => {
                          return(
                              <option key = {technician.id} value={technician.id}>
                                  {technician.first_name} {technician.last_name}
                              </option>
                          );
                      })}
                    </select>
                    <label htmlFor="Technician">Technician</label>

                  </div>
                  <button className="btn btn-primary">Create Appointment</button>
                </form>
              </div>
            </div>
          </div>

      );


}

export default AppointmentForm
