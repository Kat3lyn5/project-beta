import React, {useState, useEffect} from 'react';

function AppointmentList() {
    const[appointments, setAppointments] = useState([])

    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/')
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
    }

useEffect(() => {
    fetchData();
}, []);

return (
    <table className="table table-striped">
        <thead>
            <tr>
                <th>Date</th>
                <th>Automobile VIN</th>
                <th>Reason</th>
                <th>Technician</th>
            </tr>
        </thead>
        <tbody>
            {appointments.map((appointment) => {
                return (
                    <tr key={ appointment.pk }>
                        <td>{ appointment.date_time }</td>
                        <td>{ appointment.vin }</td>
                        <td>{ appointment.reason }</td>
                        <td>{ appointment.technician }</td>
                    </tr>
                );
            })}
        </tbody>
    </table>
);

}

export default AppointmentList
