import React, {useState, useEffect} from 'react';

function ManufacturerForm(props) {
    const [manufacturers, setManufacturers] = useState([]);

    const handleManufacturersChange = (event) => {
        const value = event.target.value;
        setManufacturers(value);
    }

    const handleSubmit = async(event) => {
        event.preventDefault();
        const data = {};

        data.name = manufacturers;

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json'
            }
        }

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
        const newManufacturer = await response.json();
        console.log(newManufacturer);

        setManufacturers('');
        }
    }
    return(
        <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create Manufacturer</h1>
                <form onSubmit={handleSubmit} id="create-manufacturer-form">
                  <div className="form-floating mb-3">
                    <input onChange= {handleManufacturersChange} value ={manufacturers} placeholder="manufacturer" required type="text" name="manufacturer" id ="manufacturer" className="form-control"/>
                    <label htmlFor="Manufacturer">Manufacturer</label>
                    </div>
                  <button className="btn btn-primary">Create Manufacturer</button>
                </form>
              </div>
            </div>
          </div>

      );

}

export default ManufacturerForm;
