import React,{ useEffect, useState } from 'react';

function SalesList() {
    const [sales, setSales] = useState([]);

    const fetchData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
    }
}

useEffect(() => {
    fetchData()
  }, []);

  return (
    <table className="table table-striped">
    <thead>
      <tr>
        <th>Salesperson</th>
        <th>Employee Id</th>
        <th>Automobile</th>
        <th>Price</th>
      </tr>
    </thead>
    <tbody>
      {sales.map((sales) => {
        return (
          <tr key={ sales.id }>
            <td>{ sales.salesperson.first_name }</td>
            <td>{ sales.salesperson.employee_id }</td>
            <td>{ sales.automobile.vin }</td>
            <td>{ sales.price }</td>
          </tr>
        );
      })}
    </tbody>
  </table>
);


}



export default SalesList;
