import { useState, useEffect } from 'react';

function SalespersonHistory() {
    const [sales, setSales] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState([]);

const loadSales = async() => {
    const response = await fetch('http://localhost:8090/api/sales/');
    if (response.ok) {
      const data = await response.json();
      console.log(data)
      setSales(data.sales);
  } else(
      console.error('An error occured fetching the data')
  )

  }

const loadSalespeople = async() => {
    const response = await fetch('http://localhost:8090/api/salespeople/');
    if (response.ok) {
      const data = await response.json();
      console.log(data)
      setSalespeople(data.salespeople);
  } else(
      console.error('An error occured fetching the data')
  )
  }

  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSalesperson(value);
loadSalespeople();
  }

  const getFilteredSales = (salesperson, sales) => {
    if(!salesperson) {
        return sales;
    }
    return sales.filter(sale =>
        Object.values(sale).some(value => {
            if (typeof value === 'string') {
                return value.includes(salesperson);
            } else {
                const stringValue = value.toString();
                return stringValue.includes(salesperson);
            }
        })
    );

  };
  const SalesFiltered = getFilteredSales(salesperson,sales)


  useEffect(() => {
    loadSalespeople();
    loadSales();
  }, []);


  return (
    <div className="shadow p-4 mt-4">
        <h1>Salesperson History</h1>
          <select onChange= {handleSalespersonChange} value ={parseInt(salesperson)? 0: salesperson} placeholder="Salesperson"  name="salesperson" id ="salesperson" className="form-select">
            <option value = "0"> Filter Salesperson</option>
            {salespeople.map(salesperson => {
                return(
                    <option key = {salesperson.id} value={salesperson.id}>
                        {salesperson.first_name} {salesperson.last_name}
                    </option>
                )
            })}
        </select>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Salesperson</th>
                <th>Customer</th>
                <th>Automobile</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
            {SalesFiltered.map((sale) => {
                return (
                <tr key={ sale.id }>
                    <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                    <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                    <td>{ sale.automobile.vin }</td>
                </tr>
                );
            })}
            </tbody>
        </table>
        </div>

        );

        }

export default SalespersonHistory;
