import React, {useState, useEffect} from "react";

function TechnicianForm(props) {
    const[firstName, setFirstName] = useState('');
    const[lastName, setLastName] = useState('');
    const[employeeId, setEmployeeId] = useState('');

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    const handleSubmit = async(event) => {
        event.preventDefault();
        const data = {};

        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId;

    console.log(data);

    const technicianUrl = 'http://localhost:8080/api/technicians/'
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
        const newTechnician = await response.json();
        console.log(newTechnician);

        setFirstName('');
        setLastName('');
        setEmployeeId('');
    }
}

    useEffect(() => {

        }, []);

return(
    <div className="row">
        <div className="offset-3 col-6">
            <div className = "shadow p-4 mt-4">
                <h1>Create Technician</h1>
                <form onSubmit={handleSubmit} id="create=technician-form">
                    <div className="form-floating mb-3">
                        <input onChange= {handleFirstNameChange} value ={firstName} placeholder="First Name" required type="text" name="First Name" id ="First Name" className="form-control"/>
                        <label htmlFor="First Name">First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange = {handleLastNameChange} value={lastName}placeholder="Last Name" required type="text" name="last name" id="last name" className="form-control"/>
                        <label htmlFor="Last Name">Last Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange ={handleEmployeeIdChange} value ={employeeId}placeholder="Employee id" required type="employee id" name="employee id" id="employee id" className="form-control"/>
                        <label htmlFor="Employee Id">Employee Id</label>
                    </div>
                    <button className="btn btn-primary">Create Technician</button>
                </form>
            </div>
        </div>
    </div>

    );

}


export default TechnicianForm
