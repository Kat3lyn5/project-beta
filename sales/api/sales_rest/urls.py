from django.urls import path
from .views import api_list_customers, api_show_customers, api_list_sales, api_show_sales, api_list_salespeople, api_show_salespeople, api_list_automobileVOs

urlpatterns = [
    path("sales/", api_list_sales, name = "api_list_sales"),
    path("sales/<int:id>/", api_show_sales, name = "api_show_sales"),
    path("customers/", api_list_customers, name = "api_list_customers"),
    path("customers/<int:id>/", api_show_customers, name = "api_show_customers"),
    path("salespeople/", api_list_salespeople, name = "api_list_salespeople"),
    path("salesperson/<int:id>/", api_show_salespeople, name = "api_show_salespeople"),
    path("automobileVOs/", api_list_automobileVOs, name = "api_list_automobileVOs")


]
