from .models import AutomobileVO, Salesperson, Customer, Sale
from common.json import ModelEncoder
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods

# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",

    ]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]

class SalesEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "automobile",
        "salesperson",
        "customer",
        "id"
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }


@require_http_methods(["GET","POST"])
def api_list_sales(request):
    if request.method == "GET":
            sales = Sale.objects.all()
            return JsonResponse(
                {"sales": sales},
                encoder = SalesEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)

        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
            print(customer)
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer is not valid"},
                status = 400
            )
        try:
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson is not valid"},
                status = 400
            )

        try:
            vin = content["automobile"]
            print(vin)
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"]= automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "automobile is not valid"},
                status = 400
            )
        print(content)
        sales = Sale.objects.create(**content)
        return JsonResponse(
                {"sales": sales},
                encoder = SalesEncoder,
                safe = False,
            )



@require_http_methods(["GET","DELETE"])
def api_show_sales(request,id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
            return JsonResponse(
                sale,
                encoder = SalesEncoder,
                safe = False
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sales does not exist" },
            )
    else:
        request.method == "DELETE"
        count, _= Sale.objects.get(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET","POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder = CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customers = Customer.objects.create(**content)
        return JsonResponse(
            customers,
            encoder = CustomerEncoder,
            safe = False,
        )

@require_http_methods(["GET","DELETE"])
def api_show_customers(request, id):
    if request.method == "GET":
        try:
            customers = Customer.objects.get(id=id)
            return JsonResponse(
                {"customer": customers},
                encoder = CustomerEncoder,
                safe = False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status = 404
            )
    else:
        count, _= Customer.objects.get(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0})

@require_http_methods(["GET","POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder = SalespersonEncoder,
            safe = False
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder = SalespersonEncoder,
                safe = False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid"},
                status = 400,
            )



@require_http_methods(["GET","DELETE"])
def api_show_salespeople(request, id):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.get(id=id)
            return JsonResponse(
                {"salesperson": salespeople},
                encoder = SalespersonEncoder,
                safe = False
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson doesn't exist"},
                status = 404
            )
    elif request.method == "DELETE":
        count, _= Salesperson.objects.get(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0})

@require_http_methods(["GET"])
def api_list_automobileVOs(request):
    if request.method =="GET":
        automobileVOs = AutomobileVO.objects.all()
        return JsonResponse(
            {"AutomobileVOs": automobileVOs},
            Encoder = AutomobileVOEncoder,
            safe = False
        )
