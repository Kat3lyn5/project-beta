from common.json import ModelEncoder

from .models import Technician, AutomobileVO, Appointment


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href"]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "vin",
    ]

    def get_extra_data(self, o):
        return {"technician": o.technician.employee_id}


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "vin",
        "technician",
    ]

    encoders = {
        "technician": TechnicianDetailEncoder(),
    }
