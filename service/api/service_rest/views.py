from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Technician, AutomobileVO, Appointment # noqa

from .encoders import (AutomobileVODetailEncoder, # noqa
                       TechnicianListEncoder,
                       TechnicianDetailEncoder,
                       AppointmentListEncoder,
                       AppointmentDetailEncoder,
                       )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):

    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )

    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False
            )

        except Technician.DoesNotExist:     # noqa
            response = JsonResponse(
                {"message": "Could not add technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE"])
def show_technician(request, pk):

    if request.method == "GET":
        technician = Technician.objects.get(pk=pk)
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Technician.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def list_appointments(request):

    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )

    else:
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False
            )

        except: # noqa
            response = JsonResponse(
                {"message": "Could not add appointment"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE"])
def show_appointment(request, pk):

    if request.method == "GET":
        appointment = Appointment.objects.get(pk=pk)
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Appointment.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})
